Depuis quelques années, l'option typographie se renforce à l'école et un bon élan se fait ressentir autour du dessin de caractères.
En vue de cerner différentes pratiques contemporaines de recherche en dessin typographique et pour définir avec les étudiants le programme de l'option sur les 3 années à venir, nous organisons en cette fin février 3 journées d'ateliers et de conférences.
Dans cette perspective, les élèves travaillent depuis septembre en petits groupes sur des axes en ligne avec l'intérêt de l'école pour les pratiques expérimentales et les outils libres et pour lesquels ils ont identifié certains acteurs dont une partie sera nos invités.

Avec Frederik Berlaen, Baptiste Bernazeau, Paul Gangloff, Pierre Huyghebaert, Luuse, Julien Priez, Johannes Verwoerd.
